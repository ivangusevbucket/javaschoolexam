package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        double lastLayerLengthDouble = (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;
        int lastLayerLength = (int) Math.round(lastLayerLengthDouble);
        if (lastLayerLength != lastLayerLengthDouble || inputNumbers.stream().anyMatch(Objects::isNull)) {
            throw new CannotBuildPyramidException();
        }
        inputNumbers.sort(Comparator.naturalOrder());
        int maxSize = lastLayerLength * 2 - 1;
        int start = maxSize / 2;
        int[][] result = new int[lastLayerLength][maxSize];
        for (int i = 0, index = 0, left = start, right = start; i < lastLayerLength; i++, left--, right++) {
            for (int j = left; j <= right; j += 2) {
                result[i][j] = inputNumbers.get(index++);
            }
        }
        return result;
    }


}
