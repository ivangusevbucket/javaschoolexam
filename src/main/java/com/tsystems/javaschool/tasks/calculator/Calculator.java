package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.expression.ExpressionParser;
import com.tsystems.javaschool.tasks.calculator.expression.ParsingException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            return new ExpressionParser().parse(statement).getResult();
        } catch (ParsingException e) {
            return null;
        }
    }

}
