package com.tsystems.javaschool.tasks.calculator.expression;

/**
 * Class representing const value in mathematical expressions.
 */
public class Const extends Expression {

    private final double value;

    /**
     * Constructs {@link Const} object with specified value.
     * @param value
     */
    public Const(double value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double calc() {
        return value;
    }
}
