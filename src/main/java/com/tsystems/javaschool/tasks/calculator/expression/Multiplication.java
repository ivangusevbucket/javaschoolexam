package com.tsystems.javaschool.tasks.calculator.expression;

/**
 * Class representing binary addition in mathematical expressions.
 */
public class Multiplication extends BinaryOperation {

    /**
     * Constructs binary multiplication class with specified arguments.
     * @param left first argument.
     * @param right second argument.
     */
    public Multiplication(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double apply(double left, double right) {
        return left * right;
    }
}
