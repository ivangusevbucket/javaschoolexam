package com.tsystems.javaschool.tasks.calculator.expression;

import java.text.DecimalFormat;

/**
 * Class for representation of mathematical expressions.
 */
public abstract class Expression {

    /**
     * Calculates the expression value and returns it's {@link String} representation.
     * @return string representation of the expression value with maximum of 4 digits after point or <code>null</code> if evaluation error occurred.
     */
    public String getResult() {
        try {
            double result = calc();
            long rounded = Math.round(result);
            if (rounded == result) {
                return Long.toString(rounded);
            } else {
                return new DecimalFormat("##.####").format(result);
            }
        } catch (CalculationException e) {
            return null;
        }
    }

    /**
     * Calculates double value of expression.
     * @return
     * @throws CalculationException
     */
    protected abstract double calc() throws CalculationException;

}
