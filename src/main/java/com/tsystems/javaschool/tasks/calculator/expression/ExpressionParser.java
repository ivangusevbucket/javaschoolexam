package com.tsystems.javaschool.tasks.calculator.expression;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.EnumSet;
import java.util.Set;

/**
 * Parser for mathematical expressions.
 */
public class ExpressionParser {

    private String expression;
    private String constString;
    private Token currentToken, nextToken;
    private int index;
    private Deque<Integer> openings;

    private final Set<Token> operationsSet = EnumSet.of(Token.ADD, Token.DIV, Token.SUB, Token.MUL, Token.NEGATIVE, Token.SUB);
    private final Set<Token> binaryOperations = EnumSet.of(Token.ADD, Token.DIV, Token.SUB, Token.MUL);

    private boolean available(int count) {
        return index + count <= expression.length();
    }

    private char currentCharacter() {
        return expression.charAt(index);
    }

    private boolean isNumberCharacter(char character) {
        return Character.isDigit(character) || character == '.';
    }

    private int getNumberBorder(int start) {
        while (start < expression.length() && isNumberCharacter(expression.charAt(start))) {
            start++;
        }
        return start;
    }

    private void skipWhitespaces() {
        while (index < expression.length() && Character.isWhitespace(currentCharacter())) {
            index++;
        }
    }

    private boolean matches(String pattern) {
        if (!available(pattern.length())) {
            return false;
        }
        for (int i = 0; i < pattern.length(); i++) {
            if (expression.charAt(index + i) != pattern.charAt(i)) {
                return false;
            }
        }
        index += pattern.length();
        return true;
    }


    private Token processToken() throws ParsingException {
        skipWhitespaces();
        if (!available(1)) {
            return Token.END;
        } else if (matches("+")) {
            return Token.ADD;
        } else if (matches("-")) {
            if (currentToken == Token.CONST || currentToken == Token.CLOSING_PARENTHESIS) {
                return Token.SUB;
            } else {
                if (isNumberCharacter(currentCharacter())) {
                    int end = getNumberBorder(index);
                    constString = "-" + expression.substring(index, end);
                    index = end;
                    if (operationsSet.contains(currentToken)) {
                        throw new ParsingException("Unwrapped negative number");
                    }
                    return Token.CONST;
                }
                return Token.NEGATIVE;
            }
        } else if (matches("*")) {
            return Token.MUL;
        } else if (matches("/")) {
            return Token.DIV;
        } else if (matches("(")) {
            return Token.OPENING_PARENTHESIS;
        } else if (matches(")")) {
            if (openings.size() == 0) {
                throw new ParsingException("Closure before opening parenthesis");
            }
            return Token.CLOSING_PARENTHESIS;
        } else {
            if (isNumberCharacter(currentCharacter())) {
                int end = getNumberBorder(index);
                constString = expression.substring(index, end);
                index = end;
                return Token.CONST;
            } else {
                throw new ParsingException("Unexpected character found");
            }
        }
    }

    private void updateTokens() throws ParsingException {
        currentToken = nextToken;
        nextToken = processToken();
        if (operationsSet.contains(currentToken) && (nextToken == Token.END || nextToken == Token.CLOSING_PARENTHESIS)) {
            throw new ParsingException("Missing argument not found");
        }
        if (operationsSet.contains(currentToken) && (nextToken == Token.NEGATIVE)) {
            throw new ParsingException("Negative without parenthesis");
        }
        if (currentToken == nextToken && currentToken != Token.OPENING_PARENTHESIS && currentToken != Token.CLOSING_PARENTHESIS && currentToken != Token.END && currentToken != Token.NEGATIVE) {
            throw new ParsingException("Incorrect statement found");
        }
        if (binaryOperations.contains(currentToken) && binaryOperations.contains(nextToken)) {
            throw new ParsingException("Unable to parse sequence of binary operations");
        }
        if ((currentToken == Token.OPENING_PARENTHESIS || currentToken == Token.EMPTY) && binaryOperations.contains(nextToken)) {
            throw new ParsingException("Missing argument not found");
        }
    }

    private Expression buildUnary() throws ParsingException {
        Expression result;
        updateTokens();
        switch (currentToken) {
            case CONST:
                try {
                    result = new Const(Double.parseDouble(constString));
                } catch (NumberFormatException e) {
                    throw new ParsingException();
                }
                updateTokens();
                break;
            case NEGATIVE:
                result = new Negative(buildUnary());
                break;
            case OPENING_PARENTHESIS:
                openings.push(index - 1);
                result = buildAddSub();
                if (currentToken != Token.CLOSING_PARENTHESIS) {
                    throw new ParsingException("Missing closure parenthesis");
                } else {
                    openings.pop();
                    updateTokens();
                }
                break;
            default:
                throw new ParsingException("Invalid syntax found");
        }
        return result;
    }

    private Expression buildMulDiv() throws ParsingException {
        Expression result = buildUnary();
        while (true) {
            switch (currentToken) {
                case MUL:
                    result = new Multiplication(result, buildUnary());
                    break;
                case DIV:
                    result = new Division(result, buildUnary());
                    break;
                default:
                    return result;
            }
        }
    }

    private Expression buildAddSub() throws ParsingException {
        Expression result = buildMulDiv();
        while (true) {
            switch (currentToken) {
                case ADD:
                    result = new Add(result, buildMulDiv());
                    break;
                case SUB:
                    result = new Subtraction(result, buildMulDiv());
                    break;
                default:
                    return result;
            }
        }
    }


    /**
     * Returns a new {@link Expression} initialized to be representation of the specified string.
     * @param expression string to be parsed
     * @return {@link Expression} object representing specified string
     * @throws ParsingException if expression is <code>null</code> or contains syntax error
     */
    public Expression parse(String expression) throws ParsingException {
        if (expression == null) {
            throw new ParsingException("Unable to parse null-expression");
        }
        this.expression = expression;
        currentToken = Token.EMPTY;
        nextToken = Token.END;
        index = 0;
        openings = new ArrayDeque<>();
        nextToken = processToken();
        Expression result = buildAddSub();
        if (currentToken == Token.CLOSING_PARENTHESIS) {
            throw new ParsingException("Closure before opening parenthesis");
        }
        return result;
    }


    private enum Token {
        EMPTY,
        CONST,
        NEGATIVE,
        ADD,
        SUB,
        MUL,
        DIV,
        OPENING_PARENTHESIS,
        CLOSING_PARENTHESIS,
        END
    }

}
