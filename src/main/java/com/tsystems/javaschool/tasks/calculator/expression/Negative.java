package com.tsystems.javaschool.tasks.calculator.expression;

/**
 * Class representing multiplication of expression on <code>-1</code> in mathematical expressions.
 */
public class Negative extends Expression {

    private final Expression expression;

    /**
     * Constructs expression representing the negative version of specified argument.
     * @param expression expression to be transformed.
     */
    public Negative(Expression expression) {
        this.expression = expression;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double calc() throws CalculationException {
        return (-1) * expression.calc();
    }
}
