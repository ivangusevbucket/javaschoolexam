package com.tsystems.javaschool.tasks.calculator.expression;

/**
 * Exception representing the parsing error for {@link ExpressionParser}.
 */
public class ParsingException extends Exception {

    /**
     * Constructs parsing exception.
     */
    public ParsingException() {
        super("Unable to parse expression");
    }

    /**
     * Constructs exception containing details to occurred error.
     * @param cause details to the exception.
     */
    public ParsingException(String cause) {
        super("Unable to parse expression: " + cause);
    }

}
