package com.tsystems.javaschool.tasks.calculator.expression;

/**
 * Exception representing mathematical error during evaluation double value of {@link Expression}
 */
public class CalculationException extends Exception {

    /**
     * Constructs exception with specified cause.
     * @param cause message with details to exception.
     */
    public CalculationException(String cause) {
        super("Calculation error: " + cause);
    }

}
