package com.tsystems.javaschool.tasks.calculator.expression;

/**
 * Class representing binary division in mathematical expressions.
 */
public class Division extends BinaryOperation {

    /**
     * Constructs binary division class with specified arguments.
     * @param left first argument.
     * @param right second argument.
     */
    public Division(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double apply(double left, double right) throws CalculationException {
        if (right == 0) {
            throw new CalculationException("Division by zero");
        } else {
            return left / right;
        }
    }
}
