package com.tsystems.javaschool.tasks.calculator.expression;

public abstract class BinaryOperation extends Expression {

    private final Expression left, right;

    /**
     * Constructs expression representing binary operation.
     * @param left first argument of binary operation.
     * @param right first argument of binary operation.
     * */
    public BinaryOperation(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double calc() throws CalculationException {
        return apply(left.calc(), right.calc());
    }

    /**
     * Evaluates application of binary operation to double arguments.
     * @param left first argument.
     * @param right second argument.
     * @return double value representing the result of application the binary operation to specified arguments.
     * @throws CalculationException if evaluation error occurred.
     */
    protected abstract double apply(double left, double right) throws CalculationException;

}
