package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("Not null arguments required");
        }
        int x_iterator = 0;
        for (int y_iterator = 0; x_iterator < x.size() && y_iterator < y.size(); y_iterator++) {
            if (x.get(x_iterator).equals(y.get(y_iterator))) {
                x_iterator++;
            }
        }
        return x_iterator == x.size();
    }
}
